from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutme', views.aboutme, name='aboutme'),
    path('aboutme2', views.aboutme2, name='aboutme2'),
    path('books', views.books, name='books'),
    path('movies', views.movies, name='movies'),
    path('books/ayah', views.ayah, name='ayah'),
    path('movies/knives-out', views.knivesout, name='knivesout'),
]