from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'home.html')
def aboutme(request):
    return render(request, 'aboutme.html')
def aboutme2(request):
    return render(request, 'aboutme2.html')
def books(request):
    return render(request, 'books.html')
def movies(request):
    return render(request, 'movies.html')
def ayah(request):
    return render(request, 'ayah.html')
def knivesout(request):
    return render(request, 'knivesout.html')